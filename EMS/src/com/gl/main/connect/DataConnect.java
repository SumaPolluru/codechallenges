package com.gl.main.connect;

import java.sql.*;

public class DataConnect {
	private static Connection con;

	// This interface is responsible for establishing database connection
	private DataConnect() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			// In java 8 it will be com.mysql.cj.jdbc.Driver
			con = DriverManager.getConnection("jdbc:mysql://localhost:3306/hcl_training", "root", "@Chintu12345");
			System.out.println("Connection established");
			// This statement is equivalent to Driver d1=new Driver();
			// It will load the Driver class dynamically

			/*
			 * jdbc--protocol mysql-subprotocol localhost--current system 3306-port no of
			 * mysql root-username for mysql mysql-password
			 */
		} catch (Exception e) {
			System.out.println("Exception is " + e.getMessage());
		}
	}

	public static Connection getConnect() {
		DataConnect d1 = new DataConnect();
		return con;
	}

	public static void main(String[] args) {
		getConnect();
	}

}
