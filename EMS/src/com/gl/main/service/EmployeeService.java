package com.gl.main.service;
public interface EmployeeService {
	public void insertEmployee();
	public void deleteEmployee();
	public void updateEmployee();
	public void retreiveEmployee();
	public void displayItems();
	
}
