package com.gl.main.service;

import com.gl.main.dao.*;
import java.util.Scanner;

import com.gl.main.pojo.Employee;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class EmployeeServiceImpl implements EmployeeService {
	private Scanner sc;
	Employee emp;
	EmployeeDAOImpl daoimpl;

	public EmployeeServiceImpl() {
		sc = new Scanner(System.in);
		emp = new Employee();
		daoimpl = new EmployeeDAOImpl();
	}

	@Override
	public void insertEmployee() {
		System.out.println("Enter no of employees u want to add ");
		int noofemployees = sc.nextInt();
		for (int x = 1; x <= noofemployees; x++) {
			Employee e1 = new Employee();
			System.out.println("Enter Employee code");
			e1.setEmployeeid(sc.nextInt());
			System.out.println("Enter Employee Name ");
			e1.setEmployeename(sc.next());
			System.out.println("Enter Employee Address ");
			e1.setAddress(sc.next());
			System.out.println("Enter Qualification ");
			e1.setQualification(sc.next());
			System.out.println("Enter Salary ");
			e1.setSalary(sc.nextDouble());
			System.out.println("Enter Mobile no");
			e1.setMobileno(sc.next());
			System.out.println("Enter dept code");
			e1.setDeptcode(sc.nextInt());
			System.out.println("Enter Department code ");
			e1.setDepartmentcode(sc.nextInt());
			daoimpl.insert(e1);
		}

	}

	@Override
	public void deleteEmployee() {
		System.out.println("Enter Employee code which u want to delete");
		int empid = sc.nextInt();
		Employee deleteemployee = new Employee();
		deleteemployee.setEmployeeid(empid);
		// updateemployee.setEmployeeid();
		daoimpl.deleteDetails(deleteemployee);

	}

	@Override
	public void updateEmployee() {
		System.out.println("Enter Employee code which u want to update");
		int empid = sc.nextInt();
		System.out.println("Enter new Salary ");
		double newsalary = sc.nextDouble();
		Employee updateemployee = new Employee();
		updateemployee.setEmployeeid(empid);
		updateemployee.setSalary(newsalary);
		daoimpl.updateDetails(updateemployee);
	}

	@Override
	public void retreiveEmployee() {
		List<Employee> elist = daoimpl.retreiveData();
		for (Employee e1 : elist) {
			System.out.println("Employee name is " + e1.getEmployeename());
			System.out.println("Employee salary is " + e1.getSalary());
		}

	}
	@Override
	public void displayItems()
	{
		List<Employee> emplist = daoimpl.displayItems(emp);
		for (Employee obj : emplist) {
			System.out.println("Employee name is " + obj.getEmployeename());
			System.out.println("Employee salary is " + obj.getSalary());
		}
		}
}