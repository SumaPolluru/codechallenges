package com.gl.main.dao;

import java.util.List;

import com.gl.main.pojo.Department;
import com.gl.main.pojo.Employee;

public interface EmployeeDAO
{
		public void insert(Employee emp);
		public void updateDetails(Employee eupdate);
		public void deleteDetails(Employee edelete);
		public List<Employee> retreiveData();
		public List<Employee> displayJoin();
		public List<Employee> displayItems(Employee obj);

	}

