package com.gl.main.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import com.gl.main.connect.*;
import com.gl.main.pojo.Department;
import com.gl.main.pojo.Employee;

public class EmployeeDAOImpl implements EmployeeDAO {
	private Connection con1;
	private PreparedStatement stat;// PreparedStatement is and interface in java.sql
	// which can be used to write queries using java to database.

	public EmployeeDAOImpl() {
		con1 = DataConnect.getConnect();
	}

	@Override
	public void insert(Employee emp) {
		try {
			stat = con1.prepareStatement("insert into employee values(?,?,?,?,?,?,?,?)");
			stat.setInt(1, emp.getEmployeeid());
			stat.setString(2, emp.getEmployeename());
			stat.setDouble(3, emp.getSalary());
			stat.setString(4, emp.getAddress());
			stat.setString(5, emp.getQualification());
			stat.setString(6, emp.getMobileno());
			stat.setInt(7, emp.getDeptcode());
			stat.setInt(8, emp.getDepartmentcode());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data inserted");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	@Override
	public void updateDetails(Employee eupdate) {
		try {
			stat = con1.prepareStatement("update employee set salary=? where employeeid=?");
			stat.setDouble(1, eupdate.getSalary());
			stat.setInt(2, eupdate.getEmployeeid());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data updated success");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}
	}

	@Override
	public void deleteDetails(Employee edelete) {
		try {
			stat = con1.prepareStatement("delete from employee where employeeid=?");
			stat.setInt(1, edelete.getEmployeeid());
			int result = stat.executeUpdate();
			if (result > 0) {
				System.out.println("Data deletion success");
			}
		} catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

	@Override
	public List<Employee> retreiveData() {
		List<Employee> emplist = new ArrayList<Employee>();
		try {
			stat = con1.prepareStatement("select * from employee");

			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Employee e1 = new Employee();
				e1.setEmployeeid(result.getInt(1));
				e1.setEmployeename(result.getString(2));
				e1.setSalary(result.getDouble(3));
				e1.setAddress(result.getString(4));
				e1.setQualification(result.getString(5));
				e1.setMobileno(result.getString(6));
				e1.setDeptcode(result.getInt(7));
				e1.setDepartmentcode(result.getInt(8));

				emplist.add(e1);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emplist;
	}
	@Override
	public List<Employee> displayJoin() {
		List<Employee> emplist = new ArrayList<Employee>();
		List<Department> list=new ArrayList<Department>();
		try {
			stat = con1.prepareStatement("select employeeId e,employeename e,deptcode d,deptname d from employee e,department d where d.deptcode=e.deptcode");
			ResultSet result = stat.executeQuery();
			while (result.next()) {
				Employee e1 = new Employee();
				e1.setEmployeeid(result.getInt(1));
				e1.setEmployeename(result.getString(2));
				e1.setDepartmentcode(result.getInt(3));
				Department e2=new Department();
				e2.setDeptname(result.getNString(4));
				emplist.add(e1);
				list.add(e2);

			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return emplist;
	}

	@Override
	public List<Employee> displayItems(Employee obj) {
		List<Employee> emplist = new ArrayList<Employee>();
		try {
			stat = con1.prepareStatement("select employeeid, employeename from employee where address=?");
			stat.setString(1, obj.getAddress());
			 ResultSet result = stat.executeQuery();			
				while(result.next()) {
					Employee e=new Employee();
					 e.setEmployeeid(result.getInt(1));
					e.setEmployeename(result.getString(2));	
					emplist.add(e);
				}
	} catch (SQLException e) {
	// TODO Auto-generated catch block
	e.printStackTrace();
}
		return emplist;
}
}
