package com.gl.main.pojo;

public class Department {
	private int deptcode;
	private String deptname;
	private String City;
	private String departmenthead;

	public int getDeptcode() {
		return deptcode;
	}

	public void setDeptcode(int deptcode) {
		this.deptcode = deptcode;
	}

	public String getDeptname() {
		return deptname;
	}

	public void setDeptname(String deptname) {
		this.deptname = deptname;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public String getDepartmenthead() {
		return departmenthead;
	}

	public void setDepartmenthead(String departmenthead) {
		this.departmenthead = departmenthead;
	}

}
