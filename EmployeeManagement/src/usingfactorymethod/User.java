package usingfactorymethod;

public abstract class User {
	public void acceptDetails()
	{
		System.out.println("Accepting Details Of User");
	}
	public void displayDetails()
	{
		System.out.println("Displaying Details Of User ");
	}
	public abstract void userOperation();

}
