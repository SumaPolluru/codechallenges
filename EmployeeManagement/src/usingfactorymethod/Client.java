package usingfactorymethod;

public class Client extends User{

	@Override
	public void userOperation() {
			System.out.println("Performing Client Operations");
	}

}
