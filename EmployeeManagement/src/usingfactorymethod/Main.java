package usingfactorymethod;
import java.util.*;
public class Main {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Enter User type Client/Visitor");
		String name=sc.next();
		GetUserFactory empfact=new GetUserFactory();
		User eobj=empfact.getUser(name);
		eobj.acceptDetails();
		eobj.displayDetails();
		eobj.userOperation();
	}

}
