package userInfo.pjo;

import java.time.LocalDate;
import java.time.LocalTime;

public class Task {
		private Integer taskId;
		private String taskTitle;
		private String taskText;
		private String assignTo;
		private LocalDate date;
		public LocalTime time;
		public LocalDate setDate;
		public LocalDate completionDate;
		public LocalTime completionTime;
public Task() 
{
	
}
public Task(Integer taskId, String taskTitle, String taskText, String assignTo)
{
	super();
	this.taskId = taskId;
	this.taskTitle = taskTitle;
	this.taskText = taskText;
	this.assignTo = assignTo;
}

		public void setTaskId(Integer taskId)
		{
			this.taskId=taskId;
			
		}
		public Integer getTaskId()
		{
			return this.taskId;
		}
		public void setTaskTitle(String taskTitle)
		{
			this.taskTitle=taskTitle;
		
		}
		public String getTaskTitle()
		{
			return this.taskTitle;
		}
		public void setTaskText(String taskText)
		{
			this.taskText=taskText;
		
		}
		public String getTaskText()
		{
			return this.taskText;
		}
		public void setAssignTo(String assignTo)
		{
			this.assignTo=assignTo;
		
		}
		public String getAssignTo()
		{
			return this.assignTo;
		}	
		public LocalDate getDate() {
			return date;
		}
		public void setDate(LocalDate date) {
			this.date = LocalDate.now();
		}
		public LocalTime getTime() {
			return time;
		}
		public void setTime(LocalTime time) {
			this.time = LocalTime.now();
		}
		public LocalDate getCompletionDate() {
			this.completionDate = LocalDate.now();
			return completionDate;
		}
		public LocalTime getCompletionTime() {
			this.completionTime = LocalTime.now();
			return completionTime;
		}
}	