package userInfo.pjo;
	public abstract class UserClass{
		private String username;
         public UserClass()
         {
	
         }
    public UserClass(String username, String userpassword)
    {
	super();
	this.username = username;
	this.userpassword = userpassword;
     }
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		public String getUserpassword() {
			return userpassword;
		}
		public void setUserpassword(String userpassword) {
			this.userpassword = userpassword;
		}
		private String userpassword;
		public abstract int getClientid();
		public abstract void setClientid(int clientid);
		public abstract int getVisitorId();
		public abstract void setVisitorId(int visitorId);
		/*public User()
		{
			System.out.println("This is parent class");
		}*/
	}
	