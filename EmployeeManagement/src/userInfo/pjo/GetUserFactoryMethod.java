package userInfo.pjo;
public class GetUserFactoryMethod {
	public UserClass getUser(String emptype)
	{
		if(emptype.equals("Client"))
		{
			return new ClientData();
		}
		else if(emptype.equals("Visitor"))
		{
			return new VisitorData();
		}
		return null;

	}
}

