package userInfo.dao;

import exceptionhandling.task.CustomException;

public interface ClientDAO
{
	public void insertTask() throws CustomException;
	public void assignTo();
	public void completionDate();

}