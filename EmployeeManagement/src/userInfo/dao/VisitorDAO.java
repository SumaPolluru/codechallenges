package userInfo.dao;
public interface VisitorDAO {
public void seeTask();
public void markTask();
public void completedTasks();
public void incompleteTasks();
}
