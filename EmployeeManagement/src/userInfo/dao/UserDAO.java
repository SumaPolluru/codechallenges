package userInfo.dao;
public interface UserDAO{
public abstract void insertDetails();
public abstract void deleteDetails();
public abstract void updateDetails();
public abstract void displayDetails();
public abstract void searchDetails();
public abstract boolean checkUser(String username,String password);
}
