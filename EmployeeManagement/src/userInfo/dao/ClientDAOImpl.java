package userInfo.dao;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import exceptionhandling.task.CustomException;
import userInfo.pjo.Task;
public class ClientDAOImpl implements ClientDAO{
	private Scanner sc;
	private List<Task> tasklist;
	public ClientDAOImpl()
	{
		sc=new Scanner(System.in);
		tasklist=new ArrayList<Task>();
	}
	@Override
	public void insertTask() throws CustomException
	{

		System.out.println("Enter no of Tasks u want to add");
		int noofemployee=sc.nextInt();
		for(int x=1;x<=noofemployee;x++)
		{
			Task t1=new Task();
			System.out.println("Enter Task id ");
			int id=sc.nextInt();
			if(id<0) {
				throw new CustomException();
			}
			t1.setTaskId(id);
			System.out.println("Enter Task Title ");
			t1.setTaskTitle(sc.next());
			System.out.println("Enter Task Text ");
			t1.setTaskText(sc.next());
			System.out.println("Enter name to whom u want to assign");
			t1.setAssignTo(sc.next());
			tasklist.add(t1);
			
		}
		
	}
	@Override
	public void assignTo() {
		System.out.println("Enter Task Id for which u want to assign task");
		int taskId=sc.nextInt();
		for(Task eobj:tasklist)
		{
			if(eobj.getTaskId()==taskId)
			{
				System.out.println("please enter the name, to whom u assign the task");
				eobj.setAssignTo(sc.next());
			}
		}
		}
	@Override
	public void completionDate() {
		System.out.println("Enter Task Id for Completion date[DD/MM/YYYY]");
		int taskId=sc.nextInt();
		for(Task eobj:tasklist)
		{
			if(eobj.getTaskId()==taskId)
			{
			System.out.println("Please enter the completion date for the task");
			String date=sc.next();
			System.out.println("The completion date for this task id is: "+date);
			}
		}
		
	}

}
