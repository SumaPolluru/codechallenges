package userInfo.dao;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

import userInfo.pjo.ClientData;
import userInfo.pjo.UserClass;
public class UserDAOImpl implements UserDAO{
		private Scanner sc;
		private List<UserClass> userlist;
		public UserDAOImpl()
		{
			sc=new Scanner(System.in);
			userlist=new ArrayList<UserClass>();
			
		}
		public void menu() {
			System.out.println("Enter choice 1 or 0");
			int choice=0;
			while(choice==1)
			{
				System.out.println("1.Add Details");
				System.out.println("2.Display Details");
				System.out.println("3.Update Details");
				System.out.println("4.Delete Details");
				System.out.println("5.Search Details");
				int option=sc.nextInt();
				switch(option)
				{
				case 1: System.out.println("Adding details");
						insertDetails();
						break;
				case 2: System.out.println("Displaying Details");
						displayDetails();
						break;
				case 3: System.out.println("updating Details");
				        updateDetails();
				        break;
				case 4: System.out.println("Delete Details");
				        deleteDetails();
				        break;
				case 5: System.out.println("Search Details");
				        searchDetails();
				        break;
				case 0:
					     System.out.println("successfully logout");
					     System.exit(0);
					     break;
				default :System.out.println("invalid case");
				}
				System.out.println("Do you want to continue[0/1]");
				System.out.println("For Exit,press Zero");
				choice=sc.nextInt();
			}
		}
		public void insertDetails() {
			System.out.println("Inserting here");
			System.out.println("Enter no of users u want to add");
			int noofusers=sc.nextInt();
			for(int x=1;x<=noofusers;x++)
			{
				UserClass us=new ClientData();
				System.out.println("Enter Username ");
				us.setUsername(sc.next());
				System.out.println("Enter Password ");
				us.setUserpassword(sc.next());
				userlist.add(us);
				
			}
			
		}
		public void displayDetails()
		{
		Comparator<UserClass> sortingbyusername=(user1,user2)->
			{
				return (user1.getUsername()).compareTo(user2.getUsername());
			};
			Collections.sort(userlist,sortingbyusername);
			System.out.println("Displaying the user data in ascending order");
			userlist.forEach(
					e->
					{
						System.out.println("Username:  "+e.getUsername()+"        password:  "+e.getUserpassword());	
					});
		}
		public void updateDetails() {
			System.out.println("Updating here"); 
			System.out.println("Enter username for which u want to make updation");
				String name=sc.next();
				for(UserClass eobj:userlist)
				{
					if(eobj.getUsername().equals(name))
					{
						System.out.println("Enter New username");
						eobj.setUsername(sc.next());
						System.out.println("Enter New Password");
						eobj.setUserpassword(sc.next());
						System.out.println("For this username,The credentials are updated:"+name);
					}
				}
			
		}
		public void deleteDetails()
		{
		 System.out.println("Enter the Username which u want to delete");
			String uname=sc.next();
			boolean result=false;
			UserClass userdeleted=null;
			for(UserClass t:userlist)
			{
				if(t.getUsername().equals(uname))
				{
					result=true;
					userdeleted=t;
					System.out.println(uname+" Object removed");
				}
			}
			if(result)
			{
				userlist.remove(userdeleted);
			}
			else {
				System.out.println(uname+" Username is not present");
			}
		}
		public void searchDetails() {
			System.out.println("Enter the Username which u want to search");
			String uname=sc.next();
			boolean result=false;
			for(UserClass t:userlist)
			{
				if(t.getUsername().contains(uname))
				{
					result=true;
					System.out.println("Object searched");
				}
			}
			if(result)
			{
				System.out.println(uname+ " Username is present");
			}
			else {
				System.out.println(uname+ " Username is not present");
			}
		}
		@Override
		public boolean checkUser(String username,String password)
		{
			if(username.equals("Suma")&&password.equals("Chintu"))
			{
				return true;
			}
			else
			{
				return false;
	       }
}
		public static void main(String[] args) {
			UserDAOImpl usermain=new UserDAOImpl();
			usermain.menu();
			
		}
		
	}
