package userInfo.dao;
import exceptionhandling.task.CustomException;
public interface TaskDAO {
	public void createTask() throws CustomException;
	public void deleteTask();
	public void updateTask();
	public void displayTask();
	public void searchTask();
	public void sortTask();
	public void assignTo();
	public void completionDate();
	public void seeTask();
	public void markTask();
	public void completedTasks();
	public void incompleteTasks();
}
