package userInfo.dao;
import java.util.*;
import java.util.stream.Collectors;
import exceptionhandling.task.CustomException;
import userInfo.pjo.*;

public class TaskDAOImpl implements TaskDAO {
		private Scanner sc;
		private List<Task> tasklist;
		//Singleton Design pattern
		private static TaskDAOImpl hobj;
		public TaskDAOImpl()
		{
			sc=new Scanner(System.in);
			tasklist=new ArrayList<Task>();		
		}
		public  static TaskDAOImpl getObj()
		{
			hobj=new TaskDAOImpl();
			return hobj;
		}
		public void menu() {
			System.out.println("Enter choice 1 or 0");
			int choice =1;
			while(choice==1)
			{
				System.out.println("1.Add Details");
				System.out.println("2.Display Details");
				System.out.println("3.Update Details");
				System.out.println("4.Delete Details");
				System.out.println("5.Search Details");
				System.out.println("6.Sort the task text");
				int option=sc.nextInt();
				switch(option)
				{
				case 1: System.out.println("Adding details");
						try{
							createTask();
						}
						catch(CustomException e) {
							System.out.println(e.getMessage());
						}
						break;
				case 2: System.out.println("Displaying Details");
						displayTask();
						break;
				case 3: System.out.println("updating Details");
				        updateTask();
				        break;
				case 4: System.out.println("Delete Details");
				        deleteTask();
				        break;
				case 5: System.out.println("Search Details");
				        searchTask();
				        break;
				case 6: System.out.println("Sort the task");
				        sortTask();
				        break;
				default :System.out.println("Invalid case");
				}
				System.out.println("Do you want to continue");
				choice=sc.nextInt();
			}
		}
		public void createTask() throws CustomException{
				System.out.println("Enter no of Tasks u want to add");
				int noofemployee=sc.nextInt();
				for(int x=1;x<=noofemployee;x++)
				{
					Task t1=new Task();
					System.out.println("Enter Task id ");
					int id=sc.nextInt();
					if(id<0) {
						throw new CustomException();
					}
					t1.setTaskId(id);
					System.out.println("Enter Task Title ");
					t1.setTaskTitle(sc.next());
					System.out.println("Enter Task Text ");
					t1.setTaskText(sc.next());
					System.out.println("Enter name to whom u want to assign");
					t1.setAssignTo(sc.next());
					//System.out.println("Enter Completion date");
					tasklist.add(t1);				
				}
				
		}
		public void displayTask()
		{
			System.out.println("Using streams,Displaying all task data using distinct");
			List<Task> taskfilteredlist=tasklist.stream().distinct().
			collect(Collectors.toList());
            taskfilteredlist.forEach(prd->System.out.println("Task Id: "+prd.getTaskId()+"\nTask Title: "+prd.getTaskTitle()
            +"\nTask Text: "+prd.getTaskText()+"\nTask Assigned to: "+prd.getAssignTo()+"\nTask Completion Date: "+prd.getCompletionDate()
            +"\nTask Completion Time: "+prd.getCompletionTime()));
           
	   }
		public void updateTask()
		{
				System.out.println("Enter Task id for which u want to make updation");
				int taskid=sc.nextInt();
				for(Task eobj:tasklist)
				{
					if(eobj.getTaskId()==taskid)
					{
						System.out.println("Enter New Task Title"); 
						eobj.setTaskTitle(sc.next());
						System.out.println("Enter New Task Text"); 
						eobj.setTaskText(sc.next());
						System.out.println("Task Title and Task Text are updated for this taskid :"+taskid);
						
					}
				}
			
		}
		public void deleteTask() {
		System.out.println("Enter the task id which u want to delete");
		int taskid=sc.nextInt();
		boolean result=false;
		Task taskdeleted=null;
		for(Task t:tasklist)
		{
			if(t.getTaskId()==taskid)
			{
				result=true;
				taskdeleted=t;
				System.out.println(taskid+" Object removed");
			}
		}
		if(result)
		{
			tasklist.remove(taskdeleted);
		}
		else {
			System.out.println(taskid+" Task id is not present");
		}
	}

		public void searchTask()
		{
			System.out.println("Enter the TaskTitle which u want to search");
			String tasktitle=sc.next();
			boolean result=false;
			for(Task t:tasklist)
			{
				if(t.getTaskTitle().contains(tasktitle))
				{
					result=true;
					System.out.println(tasktitle+" Object searched");
				}
			}
			if(result)
			{
				System.out.println(tasktitle+ " TaskTitle is present");
			}
			else {
				System.out.println(tasktitle+ " TaskTitle is not present");
			}
		}
		@Override
		public void sortTask() {
			System.out.println("Ascending order of task title");
			for(Task eobj:tasklist)
			{
			String[] arr={eobj.getTaskTitle()};					
			Arrays.sort(arr);
			System.out.println(Arrays.toString(arr));
			}
			}
			
		@Override
		public void assignTo() {
			System.out.println("Enter Task Id for which u want to assign task");
			int taskId=sc.nextInt();
			for(Task eobj:tasklist)
			{
				if(eobj.getTaskId()==taskId)
				{
					System.out.println("please enter the name, to whom u assign the task");
					eobj.setAssignTo(sc.next());
				}
			}
			}
		@Override
		public void completionDate() {
			System.out.println("Enter Task Id for Completion date");
			int taskId=sc.nextInt();
			for(Task eobj:tasklist)
			{
				if(eobj.getTaskId()==taskId)
				{
				System.out.println("Completion date for this taskid "+eobj.getCompletionDate());
				}
			}		
		}
		@Override
		public void markTask() {
			System.out.println("please enter task id");
			int taskid=sc.nextInt();
			System.out.println("please enter 0/1 to mark the status");
			System.out.println("Mark 0: When task is completed");
			System.out.println("Marks 1: When task is incomplete");
			int option=sc.nextInt();
			switch(option) {
			case 0: System.out.println("The Task is completed for this taskid "+taskid);
			         break;
			case 1: System.out.println("The Task is Incompletefor this taskid "+taskid);
			        break;
			 default:
				     System.out.println("Enter valid number");
			}
		}
		@Override
		public void completedTasks() {
		 System.out.println("Completed tasks are:");
			
		}
		@Override
		public void incompleteTasks() {
			System.out.println("Incomple tasks are:");
		}
		@Override
		public void seeTask() {
		System.out.println("Please enter the taskid");
		int taskId=sc.nextInt();
		for(Task eobj:tasklist)
		{
			if(eobj.getTaskId()==taskId)
			{			
				System.out.println("clean the room"+taskId);
			}
		}
			
		}
		public static void main(String[] args) {
			TaskDAOImpl taskmain=new TaskDAOImpl();
			taskmain.menu();
			
		}
}