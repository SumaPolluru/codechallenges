package mainpackage;
import java.io.IOException;
import java.util.*;
import exceptionhandling.task.CustomException;
import logpackage.LogClass;
import userInfo.dao.*;
import userInfo.pjo.GetUserFactoryMethod;
public class MainClass extends Thread {
	static Scanner sc;
	public MainClass()
	{
		sc=new Scanner(System.in);
	}
	public void run()
	{  
		clientmenu();
		visitormenu();
	    usermenu();
	}
	public static void main(String[] args) throws SecurityException, IOException {
		TaskDAOImpl h=TaskDAOImpl.getObj();
		Thread t1=new Thread();
		t1.start();
		LogClass log=new LogClass();
		try {
			log.logMethod();
		}
		catch(Exception e) {
			System.out.println("Exception occurs");
		}
		MainClass mainobj=new MainClass();	
		UserDAOImpl cobj=new UserDAOImpl();
		String ch="null";
		int choice=0;
		do {
			System.out.println("Please enter you are belongs to");
			System.out.println("1.Client");
			System.out.println("2.Visitor");
			System.out.println("3.User");	
		    choice=sc.nextInt();
		    switch(choice)
		    {
		    case 1:
				System.out.println("!---------WELCOME CLIENT----------!");
				System.out.println("Please enter username");
				String username=sc.next();
				System.out.println("Please enter the password");
				String password=sc.next();
				cobj.checkUser(username,password);
				mainobj.checkUser(username,password);
				mainobj.clientmenu();
				break;
		    case 2:		
				System.out.println("!------WELCOME VISITOR---------!");
				System.out.println("Register for entry");
				System.out.println("Please enter your name");
				String visname=sc.next();
				System.out.println("please enter the current time");
				String time=sc.next();
		        System.out.println("You are ready now!");
		        System.out.println("Visitor Operations");
		        mainobj.visitormenu();
		        break;
		    case 3:
		    	System.out.println("!------WELCOME USER--------!");
		    	System.out.println("Login credentials creation");
		    	mainobj.usermenu();
		             break;  
			
		    default:
				System.out.println("Not allowed");
		    }
		    System.out.println("would u like to continue to Client/Visitor/User [y/n]");
		    ch=sc.next();
			}while(ch.equals("y"));
		}

	public void checkUser(String uname,String pswd)
	{
		if(uname.equals("Suma")&&pswd.equals("Chintu"))
		{
			System.out.println("Successfully login");
			System.out.println("Task Operations");
		}
		else
		{
			System.out.println("Invalid credentials");
		}
	}

	public void visitormenu(){
		TaskDAOImpl pdao=new TaskDAOImpl();
		String ch="y";
		while(ch.equals("y"))
		{ 
			System.out.println("1. See the Task ");
			System.out.println("2. Mark the status of Task");
			System.out.println("3. Completed Tasks");
			System.out.println("4. Incomplete Tasks");
			System.out.println("5. Exit");
			int choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				    pdao.seeTask();
				    break;
			case 2:
					pdao.markTask();
					break;
			case 3:
					pdao.completedTasks();
					break;
			case 4:
					pdao.incompleteTasks();
					break;
			case 5:
				 System.out.println("Successfully logout, Thank You!");
					//System.exit(0);
					break;	
			default:
				   System.out.println("Invalid option");
			}
		System.out.println("Do u want to continue to visitor operations[y/n]");
		ch=sc.next();
		}
		}
	public void clientmenu()
	{
	TaskDAOImpl pdao=new TaskDAOImpl();
	String ch="y";
	while(ch.equals("y"))
	{
		System.out.println("1. Create Task details");
		System.out.println("2. Update Task details");
		System.out.println("3. Delete Task details");
		System.out.println("4. Search the Task details");
		System.out.println("5. Display Task details");
		System.out.println("6. Assign the Task");
		System.out.println("7. Completion Date for the task");
		System.out.println("8. Sort the Task");
		System.out.println("9. Exit");
		int choice=sc.nextInt();
		switch(choice)
		{
		case 1:
			try {
				pdao.createTask();
				}
			      catch(CustomException e) {
			    	  System.out.println(e.getMessage());
			      }			
			           break;
		case 2:
				pdao.updateTask();
				break;
		case 3:
				pdao.deleteTask();
				break;
		
		case 4:
			    pdao.searchTask();
			      break;
		case 5:
				pdao.displayTask();
				break;
		case 6: pdao.assignTo();
		        break;
		case 7: pdao.completionDate();
		        break;
		case 8: pdao.sortTask();
		         break;
		case 9:		
			 System.out.println("Successfully logout, Thank You!");
				//System.exit(0);
				break;		
	    default:
	    	 System.out.println("Invalid");
		}
	System.out.println("Do u want to continue to task operations[y/n]");
	ch=sc.next();
	}
	}
		public void usermenu()
		{
		UserDAOImpl pdao=new UserDAOImpl();
		//GetUserFactoryMethod fobj=new GetUserFactoryMethod();
		String ch="y";
		while(ch.equals("y"))
		{
			System.out.println("1. Insert user Details");
			System.out.println("2. Update user Details");
			System.out.println("3. Delete user Details");
			System.out.println("4. Search user Details");
			System.out.println("5. Display user Details");
			System.out.println("6. Exit");
			int choice=sc.nextInt();
			switch(choice)
			{
			case 1:
				    pdao.insertDetails();
				    break;
			case 2:
					pdao.updateDetails();
					break;
			case 3:
					pdao.deleteDetails();
					break;
			case 4:
			    	pdao.searchDetails();
			    	break;
			case 5:
					pdao.displayDetails();
					break;
			case 6:
				 System.out.println("Successfully logout, Thank You!");
					//System.exit(0);
					break;	
			default:
				   System.out.println("Invalid option");
			}
		System.out.println("Do u want to continue to user operations[y/n]");
		ch=sc.next();
		}
		}
	}


