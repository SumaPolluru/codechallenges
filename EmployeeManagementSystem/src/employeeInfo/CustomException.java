package employeeInfo;

public class CustomException extends Exception{
	@Override
	public String getMessage()
	{
		return "Employee ID should not be negative";
	}
}
