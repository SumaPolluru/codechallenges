package employeeInfo;
public class Employee 
{
	private int empId;
	private String empName;
	private float empSalary;
	private long empContactNo;
	private String empEmail;
public int getEmpId() 
{
	return empId;
}
public void setEmpId(int empId)
{
	this.empId = empId;
}
public String getEmpName() 
{
	return empName;
}
public void setEmpName(String empName) 
{
	this.empName = empName;
}
public float getEmpSalary()
{
	return empSalary;
}
public void setEmpSalary(float empSalary) 
{
	this.empSalary = empSalary;
}
public long getEmpContactNo() 
{
	return empContactNo;
}
public void setEmpContactNo(long empContactNo)
{
	this.empContactNo = empContactNo;
}
public String getEmpEmail() 
{
	return empEmail;
}
public void setEmpEmail(String empEmail)
{
	this.empEmail = empEmail;
}

}
