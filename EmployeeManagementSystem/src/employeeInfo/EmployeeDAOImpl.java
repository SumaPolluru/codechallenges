package employeeInfo;
import java.util.*;
public class EmployeeDAOImpl implements EmployeeDAO {
	private Scanner sc;
	Employee employearr[];
	public EmployeeDAOImpl()
	{
		sc=new Scanner(System.in);
		employearr=new Employee[5];	
	}
	public void menu() 
	{
	EmployeeDAOImpl emobj=new EmployeeDAOImpl();
	String ch="y";
	while(ch.equals("y"))
	{
		System.out.println("1.Add the employee details");
		System.out.println("2.Search for Employee details");
		System.out.println("3.Edit Employee details");
		System.out.println("4.Delete Employee details");
		System.out.println("5.Display the details of  all Employees");
		System.out.println("6.Exit");
		System.out.println("please enter the operation number,which You want to perform");
		int value=sc.nextInt();
		switch(value) {
		       
		case 1: 
			   emobj.addDetails();
			   break;
		case 2:
			   emobj.searchDetails();
			   break;
		case 3:
			    emobj.editDetails();
			    break;
		case 4:
			    emobj.deleteDetails();
			    break;
		case 5:
			    emobj.displayDetails();
			    break;
		case 6:
			    System.out.println("Successfully logout");
			    System.out.println("Thanks for the using this application");
			    System.exit(0);
			    break;
	   default:
		      System.out.println("Invalid option");
		}
		System.out.println("Do you want to continue[y/n]");
		ch=sc.next();
	}
	}
	@Override
	public void addDetails() {
	System.out.println("--------Welcome to add details!-----------");
	{
	for(int index=0;index<employearr.length;index++) {
			employearr[index]=new Employee();
			System.out.println("Enter Employee id");
			employearr[index].setEmpId(sc.nextInt());
			System.out.println("Enter Employee Name ");
			employearr[index].setEmpName(sc.next());
			System.out.println("Enter Employee Salary ");
			employearr[index].setEmpSalary(sc.nextFloat());
			System.out.println("Enter Employee Contact no");
			employearr[index].setEmpContactNo(sc.nextLong());
			System.out.println("Enter Employee Email");
			employearr[index].setEmpEmail(sc.next());
			
		}

	System.out.println("Successfully added!");
	}
	}
	@Override
	public void searchDetails() {
		System.out.println("Enter EmployeeId");
		int emId=sc.nextInt();
		boolean x=false;
		for(Employee obj:employearr)
		{
			if(obj.getEmpId()==emId)
			{
				x=true;
			}
		}
		if(x)
		{
			System.out.println("Found in Employee data");
		}
		else
		{
			System.out.println("Not found in Employee data");
		}	
		
	}

	@Override
	public void editDetails() {
	System.out.println("Please enter Employee ID, whom You want to Edit");
	int Id=sc.nextInt();
	for(Employee obj:employearr) {
	if(obj.getEmpId()==Id) {
	System.out.println("Displaying the menu for edit employee details");
	System.out.println("1.Edit Employee id ");
	System.out.println("2.Edit Employee Name");
	System.out.println("3.Edit Employee salary");
	System.out.println("4.Edit Employee contact no");
	System.out.println("5.Edit Employee Email");
	System.out.println("6.Go Back!");
	System.out.println("Please enter the option,which you want edit");
	int option1=sc.nextInt();
	String ch="y";
	while(ch.equalsIgnoreCase("y")) {
	switch(option1) {
	case 1:{
		    System.out.println("please enter new employee id");
		    obj.setEmpId(sc.nextInt());
		    System.out.println("Employee id is edited");
		    break;
	       }
	case 2:{
		System.out.println("please enter new employee name");
	    obj.setEmpName(sc.next());
	    System.out.println("Employee Name is edited");
	    break;
	      }
	case 3:{
		System.out.println("please enter new employee salary");
	    obj.setEmpSalary(sc.nextFloat());
	    System.out.println("Employee Salary is edited");
	    break;
	    }
	case 4:{
		System.out.println("please enter new employee contact no.");
	    obj.setEmpContactNo(sc.nextLong());
	    System.out.println("Employee Contact no. is edited");
	    break;
	}
	case 5:{
		System.out.println("please enter new employee email");
    obj.setEmpEmail(sc.next());
    System.out.println("Employee Email is edited");
    break;
	}
	case 6:{
		System.out.println("Go back");
		System.out.println("Do u want to go back, then enter[y]");
		ch=sc.next();
	     break;
	     }
	default:
		System.out.println("Invalid option");
	}
	}
	}
	}
	}

	@Override
	public void deleteDetails() {
    System.out.println("Please enter Employee ID, whom You want to remove");
	int enteredId=sc.nextInt();
	for(Employee obj:employearr) {
	if(obj.getEmpId()==enteredId) {
		System.out.println("The entire details of employee is deleted");
	    obj.setEmpName(null);
	    obj.setEmpSalary(0);
	    obj.setEmpContactNo(0);
	    obj.setEmpEmail(null);
	}
	}
	}

	@Override
	public void displayDetails() {
		for(Employee obj:employearr)
		{
			System.out.println("-----Employee id is "+obj.getEmpId()+"-----");
			System.out.println("Employee name is "+obj.getEmpName());
			System.out.println("Employee salary is "+obj.getEmpSalary());
			System.out.println("Employee contact no. is "+obj.getEmpContactNo());
			System.out.println("Employee email is"+obj.getEmpEmail());
		}
		
	}

}
