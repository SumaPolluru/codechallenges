package employeeInfo;
public class Product {
	private String Grocery;
	private String Stationary;
	private String Toiletry;
	private String Vegetables;
	public String getGrocery() {
		return Grocery;
	}
	public void setGrocery(String grocery) {
		Grocery = grocery;
	}
	public String getStationary() {
		return Stationary;
	}
	public void setStationary(String stationary) {
		Stationary = stationary;
	}
	public String getToiletry() {
		return Toiletry;
	}
	public void setToiletry(String toiletry) {
		Toiletry = toiletry;
	}
	public String getVegetables() {
		return Vegetables;
	}
	public void setVegetables(String vegetables) {
		Vegetables = vegetables;
	}

}
