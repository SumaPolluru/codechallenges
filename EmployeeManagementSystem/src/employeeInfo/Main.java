package employeeInfo;
import java.util.Scanner;
public class Main {
static Scanner sc;
public Main() {
	sc=new Scanner(System.in);
}
public static void main(String[] args) {
	Main mobj=new Main();
	System.out.println("!------Welcome to Employee Management System----!");
	System.out.println("Welcome Admin Musca");
	System.out.println("Before accessing this application, need credentials");
	System.out.println("Please enter admin Name");
	String adminName =sc.next();
	System.out.println("Please enter password");
	String password=sc.next();
	System.out.println("Successfully logined");
	System.out.println("Displaying menu");
	System.out.println("Please enter 1 for admin operations");
	System.out.println("Please enter 2 for Product operations");
	int option=sc.nextInt();
	switch(option)
	{
	case 1:mobj.adminMenu();
	  break;
	case 2: mobj.productMenu();
	         break;
	default: System.out.println("Invalid");
     }
}
	public void adminMenu() 
	{
	EmployeeDAOImpl emobj=new EmployeeDAOImpl();
	String ch="y";
	while(ch.equals("y"))
	{
		System.out.println("1.Add the employee details");
		System.out.println("2.Search for Employee details");
		System.out.println("3.Edit Employee details");
		System.out.println("4.Delete Employee details");
		System.out.println("5.Display the details of  all Employees");
		System.out.println("6.Exit");
		System.out.println("please enter the operation number,which You want to perform");
		int value=sc.nextInt();
		switch(value) {
		       
		case 1: 
			   emobj.addDetails();
			   break;
		case 2:
			   emobj.searchDetails();
			   break;
		case 3:
			    emobj.editDetails();
			    break;
		case 4:
			    emobj.deleteDetails();
			    break;
		case 5:
			    emobj.displayDetails();
			    break;
		case 6:
			    System.out.println("Successfully logout");
			    System.out.println("Thanks for the using this application");
			    System.exit(0);
			    break;
	   default:
		      System.out.println("Invalid option");
		}
		System.out.println("Do you want to continue[y/n]");
		ch=sc.next();
	}
	}
	public void productMenu() 
	{
	ProductDAOImpl pobj=new ProductDAOImpl();
	String ch="y";
	while(ch.equals("y"))
	{
		System.out.println("1.Add the Grocery details");
		System.out.println("2.Add for Stationary details");
		System.out.println("3.Add Toiletry details");
		System.out.println("4.Add  Vegetables details");
		System.out.println("5.Exit");
		System.out.println("please enter the operation number,which You want to perform");
		int value=sc.nextInt();
		switch(value) {
		       
		case 1: 
			   pobj.addGrocery();
		        break;
			   
		case 2:
			   pobj.addStationary();
			   break;
		case 3:
			    pobj.addToiletry();
			    break;
		case 4:
			    pobj.addVegetables();
			    break;
		case 5:
			    System.out.println("Successfully logout");
			    System.out.println("Thanks for the using this application");
			    System.exit(0);
			    break;
	   default:
		      System.out.println("Invalid option");
		}
		System.out.println("Do you want to continue[y/n]");
		ch=sc.next();
	}
	}
	}


