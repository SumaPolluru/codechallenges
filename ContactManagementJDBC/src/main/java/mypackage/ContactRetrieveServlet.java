package mypackage;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dataconnect.DataConnect;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Servlet implementation class RetrieveServlet
 */
@WebServlet("/ContactRetrieveServlet")
public class ContactRetrieveServlet extends HttpServlet {
			Connection con1=DataConnect.getConnection();
			PrintWriter out;
			protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				out=response.getWriter();
				try
				{
				PreparedStatement stat=con1.prepareStatement("select * from usercontacts");
				ResultSet result=stat.executeQuery();
				out.println("<table border=3>");
				out.println("<tr><td>User Id</td>");
				out.println("<td>Contact First Name</td>");
				out.println("<td>Contact Last Name </td>");
				out.println("<td>Email</td>");
				out.println("<td>Mobile No</td>");
				out.println("<td>Date</td>");
				out.println("</tr>");
				while(result.next())
				{
					out.println("<tr>");
					out.println("<td>"+result.getInt(1)+"</td>");
					out.println("<td>"+result.getString(2)+"</td>");
					out.println("<td>"+result.getString(3)+"</td>");
					out.println("<td>"+result.getString(4)+"</td>");
					out.println("<td>"+result.getLong(5)+"</td>");
					out.println("<td>"+result.getString(6)+"</td>");
					out.println("</tr>");
				}
				out.println("</table>");
				}
				catch(SQLException ex)
				{
					System.out.println("Exception is "+ex.getMessage());
				}
		}

		
	}
