package mypackage;

import java.io.IOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dataconnect.DataConnect;
import java.sql.Connection;
import java.io.*;
/**
 * Servlet implementation class LoginServlet
 */
/*
 * it will do all work of xml enteries
 * no requirement to make any xml entries
 * it is equivlent to url pattern
 */
@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	public void doPost(HttpServletRequest req,HttpServletResponse res)throws ServletException,
	IOException
	{
		Connection con=DataConnect.getConnection();
		PrintWriter out=res.getWriter();
		String log=req.getParameter("tlogin");
		String pass=req.getParameter("tpassword");
		RequestDispatcher dispatch;
		try
		{
		PreparedStatement stat=con.prepareStatement("select * from user where username=? and password=?");
		stat.setString(1, log);
		stat.setString(2, pass);
		ResultSet result=stat.executeQuery();
		if(result.next())
		{
			dispatch=getServletContext().getRequestDispatcher("/Welcome.html");
			dispatch.forward(req, res);
					
		}
		else
		{
			dispatch=getServletContext().getRequestDispatcher("/Login.html");
			dispatch.include(req, res);
			out.println("<font color=red>Invalid id or password</font>");
		}
	}
		catch(Exception ex)
		{
			System.out.println("Excepiton is "+ex.getMessage());
		}
	}
}
