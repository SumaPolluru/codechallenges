package mypackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dataconnect.DataConnect;
import pojo.UserContacts;

/**
 * Servlet implementation class EmpUpdate
 */
@WebServlet("/ContactsUpdateServlet")
public class ContactsUpdateServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection con=DataConnect.getConnection();
	    int userId=Integer.parseInt(request.getParameter("userId"));
		String contactFirstName=request.getParameter("firstname");
		String contactLastName=request.getParameter("lastname");
		String email=request.getParameter("email");
		Long mobileno=Long.parseLong(request.getParameter("mobileno."));
		String date=request.getParameter("date");
		try
		{
		PreparedStatement stat=
				con.prepareStatement("update UserContacts set contactFirstName=?,contactLastName=?,email=?,mobileno=?,date=? where userId=?");
		stat.setInt(6,userId);
		stat.setString(1, contactFirstName);
		stat.setString(2, contactLastName);
		stat.setString(3, email);
		stat.setLong(4, mobileno);
		stat.setString(5, date);
		int result=stat.executeUpdate();
		PrintWriter out=response.getWriter();
		out.println(result>0 ? "<h2><font color=Black>Updated Succesfully</h2></font>":"<h2><font color=Black>Failed to update</h2></font>");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
	}
}