package mypackage;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.*;
import java.util.*;
@WebServlet("/header")
public class GetServletNames extends HttpServlet
{
	PrintWriter out;
	public void service(HttpServletRequest req,HttpServletResponse response)
	throws ServletException,IOException
	{
		out=response.getWriter();
		Enumeration<String> elist=req.getHeaderNames();
		out.println("<table border=2>");
		while(elist.hasMoreElements())
		{
			out.println("<tr>");
			String name =elist.nextElement();
			out.println("<td>"+name+"</td>");
			String value=req.getHeader(name);
			out.println("<td>"+value+"</td>");
			out.println("</tr>");
		}
		out.println("</table>");
		
	}
}
