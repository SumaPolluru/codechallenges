package mypackage;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dataconnect.DataConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;

/**
 * Servlet implementation class ContactDetails
 */
@WebServlet("/ContactDetails")
public class ContactDetails extends HttpServlet {

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection con = DataConnect.getConnection();
		int userId = Integer.parseInt(request.getParameter("userid"));
		String firstName = request.getParameter("firstname");
		String lastName = request.getParameter("lastname");
		String email = request.getParameter("email");
		long mobileNo = Long.parseLong(request.getParameter("mobileno."));
		String date = request.getParameter("date");
		try {
			PreparedStatement stat = con.prepareStatement("insert into usercontacts values(?,?,?,?,?,?)");
			stat.setInt(1, userId);
			stat.setString(2, firstName);
			stat.setString(3, lastName);
			stat.setString(4, email);
			stat.setLong(5, mobileNo);
			stat.setString(6, date);
			int result = stat.executeUpdate();
			PrintWriter out=response.getWriter();
			out.println(result>0 ? "<h2><font color=Black>Added Succesfully</h2></font>":"<h2><font color=Black>Failed to Add</h2></font>");
			}
		 catch (Exception ex) {
			System.out.println(ex.getMessage());
		}

	}

}
