package mypackage;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dataconnect.DataConnect;
import pojo.UserContacts;

@WebServlet("/DeleteContactsServlet")
public class DeleteContactsServlet extends HttpServlet {
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		Connection con=DataConnect.getConnection();
	    int userId=Integer.parseInt(request.getParameter("userId"));
		try
		{
		PreparedStatement stat=
				con.prepareStatement("delete from usercontacts where userId=?");
		stat.setInt(1,userId);
		int result=stat.executeUpdate();
		PrintWriter out=response.getWriter();
		out.println(result>0 ? "<h3><font color=Black>Deleted Succesfully</h3></font>":"<h3><font color=Black>Failed to delete</h3></font>");
		}
		catch(Exception ex)
		{
			System.out.println(ex.getMessage());
		}
		
	}
}