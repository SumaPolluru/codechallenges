package mypackage;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import dataconnect.DataConnect;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
/**
 * Servlet implementation class RetrieveServlet
 */

@WebServlet("/ContactRetrieveServlet")
public class ContactRetrieveServlet extends HttpServlet {
			Connection con1=DataConnect.getConnection();
			PrintWriter out;
			protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				out=response.getWriter();
				out.println("<center><h2><font color=Black>CONTACT DETAILS IN DATABASE</h2></font></center>");
				try
				{
				PreparedStatement stat=con1.prepareStatement("select * from usercontacts");
				ResultSet result=stat.executeQuery();
				out.println("<table border=3>");
				out.println("<tr><td><h3><font color=Black>User Id</td></h3></font>");
				out.println("<td><h3><font color=Black>Contact First Name</td></h3></font>");
				out.println("<td><h3><font color=Black>Contact Last Name </td></h3></font>");
				out.println("<td><h3><font color=Black>Email</td></h3></font>");
				out.println("<td><h3><font color=Black>Mobile No</td></h3></font>");
				out.println("<td><h3><font color=Black>Date</td></h3></font>");
				out.println("<td><h3><font color=Black>Update</td></h3></font>");
				out.println("<td><h3><font color=Black>Delete</td></h3></font>");
				out.println("</tr>");
				while(result.next())
				{
					out.println("<tr>");
					out.println("<td><font color=Black>"+result.getInt(1)+"</td></font>");
					out.println("<td><font color=Black>"+result.getString(2)+"</td></font>");
					out.println("<td><font color=Black>"+result.getString(3)+"</td></font>");
					out.println("<td><font color=Black>"+result.getString(4)+"</td></font>");
					out.println("<td><font color=Black>"+result.getLong(5)+"</td></font>");
					out.println("<td><font color=Black>"+result.getString(6)+"</td></font>");
					out.println("<td><a href=\"ContactsUpdate.jsp\">Update</td>");
					out.println("<td><a href=\"DeleteContacts.jsp\">Delete</td>");
					out.println("</tr>");
				}
				out.println("</table>");
				}
				catch(SQLException ex)
				{
					System.out.println("Exception is "+ex.getMessage());
				}
		}

		
	}
