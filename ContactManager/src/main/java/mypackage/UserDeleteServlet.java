package mypackage;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dataconnect.DataConnect;
import pojo.UserContacts;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@WebServlet("/UserDeleteServlet")
	public class UserDeleteServlet extends HttpServlet {
		protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			// TODO Auto-generated method stub
			Connection con=DataConnect.getConnection();
			int eid=Integer.parseInt(request.getParameter("userId"));
			try
			{
			PreparedStatement stat=con.prepareStatement
					("select userId,contactfirstname,contactlastname,mobileno from UserContacts where userId=?");
			stat.setInt(1, eid);
			ResultSet result=stat.executeQuery();
			if(result.next())
			{
				UserContacts e=new UserContacts();
				e.setUserId(result.getInt(1));
				e.setContactFirstName(result.getString(2));
				e.setContactLastName(result.getString(3));
				e.setMobileno(result.getLong(4));
				HttpSession ses=request.getSession();
				ses.setAttribute("contactdelete", e);
				RequestDispatcher dis=getServletContext().getRequestDispatcher
						("/UserDelete.jsp");			
				dis.forward(request, response);
			}
			}
			catch(Exception ex)
			{
				System.out.println(ex.getMessage());
			}		


		}

	}
