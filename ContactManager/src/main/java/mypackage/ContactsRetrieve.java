package mypackage;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dataconnect.DataConnect;
import pojo.UserContacts;

import java.sql.Connection;
import java.util.List;

@WebServlet("/ContactsRetrieve")
public class ContactsRetrieve extends HttpServlet {
			Connection con1=DataConnect.getConnection();
			PrintWriter out;
			protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
				out=response.getWriter();
				HttpSession ses=request.getSession(false);
				List<UserContacts> emplist=(List<UserContacts>)ses.getAttribute("elist");
			out.println("<table border=2>");
			out.println("<tr><td>User Id</td>");
			out.println("<td>Contact First Name</td>");
			out.println("<td>Contact Last Name</td>");
			out.println("<td>Email/td>");
			out.println("<td>MobileNo.</td>");
			out.println("<td>Date</td>");
			out.println("</tr>");
			for(UserContacts e:emplist)
			{
				out.println("<tr>");
				out.println("<td>"+e.getUserId()+"</td>");
				out.println("<td>"+e.getContactFirstName()+"</td>");
				out.println("<td>"+e.getContactLastName()+"</td>");
				out.println("<td>"+e.getEmail()+"</td>");
				out.println("<td>"+e.getMobileno()+"</td>");
				out.println("<td>"+e.getDate()+"</td>");
				out.println("</tr>");
			}
			out.println("</table>");

			}
}
