<%@page import="pojo.*"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<style>
body {
	font-family: Calibri;
	background-color: #c7ddcc;
}

input[type=text], input[type=password] {
	border: 2px solid black;
	box-sizing: border-box;
	display: inline-block;
	padding: 12px 25px;
}
</style>
<body>
	<%
	HttpSession ses = request.getSession(false);
	UserContacts e = (UserContacts) ses.getAttribute("contactdelete");
	%>
	<form action="DeleteContactsServlet" method="post">
		<table>
			<tr>
				<Td><font color=Black size=5>User Id</Td>
				</font>
				<td><input type="text" name="userId" value="<%=e.getUserId()%>" /></td>
			</tr>
			<tr>
				<td><font color=Black size=5>Contact First Name</td>
				</font>
				<td><input type="text" name="firstname"
					value="<%=e.getContactFirstName()%>" /></td>
			</tr>
			<tr>
				<td><font color=Black size=5>Contact Last Name</td>
				</font>
				<td><input type="text" name="lastname"
					value="<%=e.getContactLastName()%>" /></td>
			</tr>
			<tr>
				<td><font color=Black size=5>Mobile No</td>
				</font>
				<td><input type="text" name="mobileno."
					value="<%=e.getMobileno()%>" /></td>
			</tr>
			<tr>
				<td></td>
				<td><input type="Submit" value="Delete " />
		</table>
	</form>
</body>
</html>
